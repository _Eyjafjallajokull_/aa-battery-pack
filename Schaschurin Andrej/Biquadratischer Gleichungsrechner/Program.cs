﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biquadratischer_Gleichungsrechner
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Schaschurin Andrej IU5-32b";
            float a, b, c;
            Console.Write("Lösung für Gleichnugtyp: ax^4+bx^2+c=0\n\n");
            Console.Write("Gib der Wert a ein:\n");
            a = float.Parse(Console.ReadLine());
            Console.Write("\nGib der Wert b ein:\n");
            b = float.Parse(Console.ReadLine());
            Console.Write("\nGib der Wert c ein:\n");
            c = float.Parse(Console.ReadLine());

            while (true)
            {
                if (a == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("\nDer eingegebene Wert a ist falsch\n");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Gib der Wert a noch ein mal ein:\n");
                    a = float.Parse(Console.ReadLine());
                    continue;
                }
                float dis = (b * b) - (4 * a * c);
                float wurzeln1, wurzeln2;

                if (0 < dis)
                {
                    wurzeln1 = (-b + (float)Math.Sqrt(dis)) / (2 * a);
                    wurzeln2 = (-b - (float)Math.Sqrt(dis)) / (2 * a);
                    if ((wurzeln1 < 0 & wurzeln2 < 0) || (dis < 0))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Wurzeln existieren nicht\n");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("\nWurzeln existieren\n");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    if (wurzeln1 > 0 & wurzeln2 > 0)
                    {
                        Console.WriteLine("x1=" + "" + Math.Sqrt(wurzeln1));
                        Console.WriteLine("x2=" + "" + (-(Math.Sqrt(wurzeln1))));
                        Console.WriteLine("x3=" + "" + Math.Sqrt(wurzeln2));
                        Console.WriteLine("x4=" + "" + (-(Math.Sqrt(wurzeln2))));
                    }
                    if (wurzeln1 < 0 & wurzeln2 > 0)
                    {
                        Console.WriteLine("x1=" + "" + Math.Sqrt(wurzeln2));
                        Console.WriteLine("x2=" + "" + (-(Math.Sqrt(wurzeln2))));
                    }
                    if (wurzeln2 < 0 & wurzeln1 > 0)
                    {
                        Console.WriteLine("x1=" + "" + Math.Sqrt(wurzeln1));
                        Console.WriteLine("x2=" + "" + (-(Math.Sqrt(wurzeln1))));
                    }
                    if (wurzeln1 == 0 & wurzeln2 > 0)
                    {
                        Console.WriteLine("x1=x2" + "" + 0);
                        Console.WriteLine("x3=" + "" + Math.Sqrt(wurzeln2));
                        Console.WriteLine("x4=" + "" + (-(Math.Sqrt(wurzeln2))));
                    }
                    if (wurzeln2 == 0 & wurzeln1 > 0)
                    {
                        Console.WriteLine("x1=" + "" + Math.Sqrt(wurzeln1));
                        Console.WriteLine("x2=" + "" + (-(Math.Sqrt(wurzeln1))));
                        Console.WriteLine("x3=x4" + "" + 0);
                    }
                    if (wurzeln2 == 0 & wurzeln1 == 0)
                    {
                        Console.WriteLine("x1=x2=x3=x4=" + "" + 0);
                    }
                    if ((wurzeln2 == 0 & wurzeln1 < 0) || (wurzeln1 == 0 & wurzeln2 < 0))
                    {
                        Console.WriteLine("x1=x2=" + "" + 0);

                    }
                    break;
                }
                else if (dis == 0)
                {
                    wurzeln1 = (-b) / (2 * a);
                    Console.WriteLine("\nAlle Wurzeln sind gleich, Wurzeln sind {0}\n", wurzeln1);
                    break;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nWurzeln sind eingebildet\n");
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                }
            }
            Console.ReadKey();
            Console.WriteLine("Powered with AA Batteries");
        }
    }
}
