﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Area
{
    class Program
    {

        static double InputVal(string prompt)
        {
            double a = 0;
            do
                Console.Write(prompt);
            while (!double.TryParse(Console.ReadLine(), out a));
            return a;
        }

        public static class STATE
        {
            public const String Rectangle = "1";
            public const String Square = "2";
            public const String Circle = "3";
        }

        static string Menu()
        {
            Console.WriteLine("What area would you like to calculate?");
            Console.WriteLine("1) Rectangle;");
            Console.WriteLine("2) Square;");
            Console.WriteLine("3) Circle;");
            Console.WriteLine("e) Exit;");
            return Console.ReadLine();
        }

        static void Main(string[] args)
        {
            Console.Title = "Schaschurin Andrej IU5-32b";
            bool work = true;
            IPrint obj;
            double a1, b1;
            while (work)
            {
                switch (Menu())
                {
                    case STATE.Rectangle:
                        a1 = InputVal("Enter the height of the rectangle \n");
                        b1 = InputVal("Enter the width of the rectangle \n");
                        obj = new Rectangle(a1, b1);
                        obj.Print();
                        break;

                    case STATE.Square:
                        a1 = InputVal("Enter the height of the square \n");
                        obj = new Quadrate(a1);
                        obj.Print();
                        break;

                    case STATE.Circle:
                        a1 = InputVal("Enter circle radius \n");
                        obj = new Circle(a1);
                        obj.Print();
                        break;

                    default:
                        work = false;
                        break;
                }
                Console.WriteLine("Press any key to continue ...");
                Console.ReadKey();
                Console.Clear();
            }
        }


    }

    interface IPrint
    {
        void Print();
    }

    abstract class GeometricFigure
    {
        public GeometricFigure() { }
        public virtual double Area()
        {
            return 0;
        }
        public abstract override string ToString();
    }

    class Rectangle : GeometricFigure, IPrint
    {
        public Rectangle(double height1, double width1)
        {
            _height = height1;
            _width = width1;
        }

        private double _height = 0;
        public double height
        {
            get { return _height; }
            set { _height = value; }
        }

        private double _width = 0;
        public double width
        {
            get { return _width; }
            set { _width = value; }
        }

        public override double Area()
        {
            return _width * _height;
        }

        public override string ToString()
        {
            return "Rectangle: " + width.ToString() + "x" + height.ToString() + ", S = " + Area().ToString();
        }

        public void Print()
        {
            Console.WriteLine(this);
        }
    }

    class Quadrate : Rectangle
    {
        public Quadrate(double height1) : base(height1, height1) { }

        public override double Area()
        {
            return height * height;
        }

        public override string ToString()
        {
            return "Square: " + height.ToString() + "x" + height.ToString() + ", S = " + Area().ToString();
        }

    }

    class Circle : GeometricFigure, IPrint
    {
        public Circle(double radius)
        {
            _radius = radius;
        }

        private double _radius = 0;

        public double radius
        {
            get { return _radius; }
            set { _radius = value; }
        }

        public override double Area()
        {
            return Math.PI * _radius * _radius;
        }

        public override string ToString()
        {
            return "Circle: " + radius.ToString() + ", S = " + Area().ToString();
        }

        public void Print()
        {
            Console.WriteLine(this);
        }
    }
}