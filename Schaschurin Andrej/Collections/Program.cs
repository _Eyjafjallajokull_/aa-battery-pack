﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Schaschurin Andrej IU5-32b";
            Rectangle rect = new Rectangle(5, 4);
            Square square = new Square(5);
            Circle circle = new Circle(5);

            Console.WriteLine("\nArrayList");
            ArrayList al = new ArrayList();
            al.Add(circle);
            al.Add(rect);
            al.Add(square);
            foreach (object x in al) Console.WriteLine(x);
            Console.WriteLine("\nArrayList - Sorting");
            al.Sort();
            foreach (object x in al) Console.WriteLine(x);

            Console.WriteLine("\nList<GeometricFigure>");
            List<GeometricFigure> fl = new List<GeometricFigure>();
            fl.Add(circle);
            fl.Add(rect);
            fl.Add(square);
            foreach (GeometricFigure x in fl) Console.WriteLine(x);
            Console.WriteLine("\nList<GeometricFigure> - Sorting");
            fl.Sort();
            foreach (GeometricFigure x in fl) Console.WriteLine(x);

            Console.WriteLine("\nMatrix");
            Matrix<GeometricFigure> cube = new Matrix<GeometricFigure>(3, 3, 3, null);
            cube[0, 0, 0] = rect;
            cube[1, 1, 1] = square;
            cube[2, 2, 2] = circle;
            Console.WriteLine(cube.ToString());

            Console.WriteLine("\nList");
            SimpleList<GeometricFigure> list = new SimpleList<GeometricFigure>();
            list.Add(square);
            list.Add(rect);
            list.Add(circle);
            foreach (var x in list) Console.WriteLine(x);
            list.Sort();
            Console.WriteLine("\nList sorting");
            foreach (var x in list) Console.WriteLine(x);

            Console.WriteLine("\nStack");
            SimpleStack<GeometricFigure> stack = new SimpleStack<GeometricFigure>();
            stack.Push(rect);
            stack.Push(square);
            stack.Push(circle);
            while (stack.Count > 0)
            {
                GeometricFigure f = stack.Pop();
                Console.WriteLine(f);
            }
            Console.ReadLine();
        }


    }


    interface IPrint
    {
        void Print();
    }

    abstract class GeometricFigure : IComparable, IPrint
    {
        public GeometricFigure() { }
        public virtual double Area()
        {
            return 0;
        }
        public abstract override string ToString();
        public void Print()
        {
            Console.WriteLine(this);
        }

        public int CompareTo(object obj)
        {
            GeometricFigure p = (GeometricFigure)obj;
            if (this.Area() < p.Area()) return -1;
            else if (this.Area() == p.Area()) return 0;
            else return 1;
        }
    }

    class Rectangle : GeometricFigure
    {
        public Rectangle(double height1, double width1)
        {
            _height = height1;
            _width = width1;
        }

        private double _height = 0;
        public double height
        {
            get { return _height; }
            set { _height = value; }
        }

        private double _width = 0;
        public double width
        {
            get { return _width; }
            set { _width = value; }
        }

        public override double Area()
        {
            return _width * _height;
        }

        public override string ToString()
        {
            return "Rectangle: " + width.ToString() + "x" + height.ToString() + ", S = " + Area().ToString();
        }



    }

    class Square : Rectangle
    {
        public Square(double height1) : base(height1, height1) { }

        public override double Area()
        {
            return height * height;
        }

        public override string ToString()
        {
            return "Square: " + height.ToString() + "x" + height.ToString() + ", S = " + Area().ToString();
        }

    }

    class Circle : GeometricFigure
    {
        public Circle(double radius)
        {
            _radius = radius;
        }

        private double _radius = 0;

        public double radius
        {
            get { return _radius; }
            set { _radius = value; }
        }

        public override double Area()
        {
            return Math.PI * _radius * _radius;
        }

        public override string ToString()
        {
            return "Circle: " + radius.ToString() + ", S = " + Area().ToString();
        }

    }
    public class Matrix<T>
    {

        Dictionary<string, T> _matrix = new Dictionary<string, T>();

        int maxX;

        int maxY;

        int maxZ;

        T nullElement;

        public Matrix(int px, int py, int pz, T nullElementParam)
        {
            maxX = px;
            maxY = py;
            maxZ = pz;
            this.nullElement = nullElementParam;
        }

        public T this[int x, int y, int z]
        {
            get
            {
                CheckBounds(x, y, z);
                string key = DictKey(x, y, z);
                if (this._matrix.ContainsKey(key))
                {
                    return this._matrix[key];
                }
                else
                {
                    return this.nullElement;
                }
            }
            set
            {
                CheckBounds(x, y, z);
                string key = DictKey(x, y, z);
                this._matrix.Add(key, value);
            }
        }

        void CheckBounds(int x, int y, int z)
        {
            if (x < 0 || x >= this.maxX) throw new Exception("x=" + x + " goes beyond the border");
            if (y < 0 || y >= this.maxY) throw new Exception("y=" + y + " goes beyond the border");
            if (z < 0 || z >= this.maxY) throw new Exception("z=" + z + " goes beyond the border");
        }

        string DictKey(int x, int y, int z)
        {
            return x.ToString() + "_" + y.ToString() + "_" + z.ToString();
        }

        public override string ToString()
        {

            StringBuilder b = new StringBuilder();
            for (int k = 0; k < maxZ; k++)
            {
                b.Append("\t");
                for (int j = 0; j < maxY; j++)
                {
                    if (j > 0) b.Append("\t");
                    b.Append("[");
                    for (int i = 0; i < maxX; i++)
                    {
                        if (this[i, j, k] != null)
                            b.Append(this[i, j, k].ToString());
                        else
                            b.Append("Null");
                        if (i != (maxX - 1)) b.Append(", ");
                    }
                    b.Append("]\n");
                }
                b.Append("\n");
            }
            return b.ToString();
        }
    }

    public class SimpleListItem<T>
    {

        public T data { get; set; }

        public SimpleListItem<T> next { get; set; }

        public SimpleListItem(T param)
        {
            this.data = param;
        }
    }

    public class SimpleList<T> : IEnumerable<T>
    where T : IComparable
    {
        protected SimpleListItem<T> first = null;
        protected SimpleListItem<T> last = null;

        public int Count
        {
            get { return _count; }
            protected set { _count = value; }
        }
        int _count;

        public void Add(T element)
        {
            SimpleListItem<T> newItem = new SimpleListItem<T>(element);
            this.Count++;
            if (last == null)
            {
                this.first = newItem;
                this.last = newItem;
            }
            else
            {
                this.last.next = newItem;
                this.last = newItem;
            }
        }

        public SimpleListItem<T> GetItem(int number)
        {
            if ((number < 0) || (number >= this.Count))
            {
                throw new Exception("Going abroad index");
            }
            SimpleListItem<T> current = this.first;
            int i = 0;
            while (i < number)
            {
                current = current.next;
                i++;
            }
            return current;
        }

        public T Get(int number)
        {
            return GetItem(number).data;
        }

        public IEnumerator<T> GetEnumerator()
        {
            SimpleListItem<T> current = this.first;
            while (current != null)
            {

                yield return current.data;
                current = current.next;
            }
        }

        System.Collections.IEnumerator
       System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Sort()
        {
            Sort(0, this.Count - 1);
        }

        private void Sort(int low, int high)
        {
            int i = low;
            int j = high;
            T x = Get((low + high) / 2);
            do
            {
                while (Get(i).CompareTo(x) < 0) ++i;
                while (Get(j).CompareTo(x) > 0) --j;
                if (i <= j)
                {
                    Swap(i, j);
                    i++; j--;
                }
            } while (i <= j);
            if (low < j) Sort(low, j);
            if (i < high) Sort(i, high);
        }

        private void Swap(int i, int j)
        {
            SimpleListItem<T> ci = GetItem(i);
            SimpleListItem<T> cj = GetItem(j);

            T temp = ci.data;
            ci.data = cj.data;
            cj.data = temp;
        }
    }
    class SimpleStack<T> : SimpleList<T>
    where T : IComparable
    {

        public void Push(T element)
        {
            Add(element);
        }


        public T Pop()
        {
            T element = Get(Count - 1);

            SimpleListItem<T> listItem = GetItem(Count - 1);
            listItem = null;

            Count--;

            return element;
        }
    }
}