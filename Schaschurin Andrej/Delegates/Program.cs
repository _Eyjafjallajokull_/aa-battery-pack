﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    delegate float mulordiv(int p1, int p2);

    class Program
    {
        static float mul(int p1, int p2) { return p1 * p2; }
        static float div(int p1, int p2) { return p1 / p2; }
        static void mulordivMethod(string str, int i1, int i2, mulordiv mulordivParam)
        {
            float Result = mulordivParam(i1, i2);
            Console.WriteLine(str + Result.ToString());
        }
        static void mulordivMethodFunc(string str, int i1, int i2, Func<int, int, float> mulordivParam)
        {
            float Result = mulordivParam(i1, i2);
            Console.WriteLine(str + Result.ToString());
        }
       /* static void mulordivMethodAction(string str, int i1, int i2, Action<int, int> mulordivParam)
         {
             void Result = mulordivParam(i1, i2);
             Console.WriteLine(str + Result.ToString());
         }
         */

        static void Main(string[] args)
        {
            Console.Title = "Schaschurin Andrej IU5-32b";
            int i1 = 3;
            int i2 = 2;

            mulordivMethod("Multiplication: ", i1, i2, mul);
            mulordivMethod("Division: ", i1, i2, div);

            mulordivMethod("Creating a delegate instance based on a lambda expression 1: ", i1, i2,
            (int x, int y) =>
            {
                int z = x * y;
                return z;
            }
             );
            mulordivMethod("Creating a delegate instance based on lambda expression 2: ", i1, i2,
            (x, y) =>
            {
                return x * y;
            }
             );
            mulordivMethod("Creating a delegate instance based on lambda expression 3: ", i1, i2, (x, y) => x / y);

            Console.WriteLine("\nUsing the Generalized Func<> Delegate");
            mulordivMethodFunc("Create a delegate instance based on a method:", i1, i2, div);
            //Console.WriteLine("\nUsing a generic Action<> delegate");
            //mulordivMethodAction("Create a delegate instance based on a method:", i1, i2, div);
            Console.ReadKey();
        }
    }
}